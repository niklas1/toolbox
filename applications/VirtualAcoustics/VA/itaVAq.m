%% itaVA quick connect
va = VA( 'localhost' );

%% Also add current dir to VA search path
va.add_search_path( pwd );